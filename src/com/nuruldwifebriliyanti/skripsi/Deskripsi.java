package com.nuruldwifebriliyanti.skripsi;

import java.io.IOException;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

@SuppressLint("NewApi") 
public class Deskripsi extends AppCompatActivity implements OnClickListener {
	private final MediaPlayer mp = new MediaPlayer();
	private ImageButton btn_Id,btn_En,btn_Ar;
	private  String id_suara,en_suara,ar_suara,id_ejaan,en_ejaan,ar_ejaan; 
	TextView txt_ejaan;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.deskripsi);
		
		Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Ayo Membaca");
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		Intent intent = getIntent();
		String gambar = intent.getStringExtra("gambar");
		id_suara  = intent.getStringExtra("id_suara");
		en_suara  = intent.getStringExtra("en_suara");
		ar_suara  = intent.getStringExtra("ar_suara");
		id_ejaan  = intent.getStringExtra("id_ejaan");
		en_ejaan  = intent.getStringExtra("en_ejaan");
		ar_ejaan  = intent.getStringExtra("ar_ejaan");
		String nama  = intent.getStringExtra("nama");
		
		ImageView img = (ImageView)findViewById(R.id.deskImg);
		btn_Id = (ImageButton)findViewById(R.id.deskPlayerId);
		btn_En = (ImageButton)findViewById(R.id.deskPlayerEn);
		btn_Ar = (ImageButton)findViewById(R.id.deskPlayerAr);
		TextView txt_id = (TextView)findViewById(R.id.textId);
		TextView txt_en = (TextView)findViewById(R.id.textEn);
		TextView txt_ar = (TextView)findViewById(R.id.textAr);
		txt_ejaan = (TextView)findViewById(R.id.editTextejaan);
		if (intent != null){
			try {
				//mencari nama file gambar yang sama pada folder drawable
				int image = getApplicationContext().getResources().getIdentifier(gambar, "drawable", getApplicationContext().getPackageName());
				//menampilkan hasil pencarian gambar
				img.setBackground(getResources().getDrawable(image));
				txt_id.setText(nama);
				txt_en.setText(en_ejaan);
				txt_ar.setText(ar_ejaan);
				txt_ejaan.setText(id_ejaan);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		btn_Id.setOnClickListener(this);
		btn_En.setOnClickListener(this);
		btn_Ar.setOnClickListener(this);
		
		
		
	}
	
	
	
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		txt_ejaan.setText(id_ejaan);
		int id = arg0.getId();
		if (id == R.id.deskPlayerId){
			btn_Id.setBackgroundResource(R.drawable.btn_player_pause);
			
			try {
	            mp.reset();
	            //mencocokan nama player dari database ke folder assets
	            AssetFileDescriptor afd = getAssets().openFd("audio/"+id_suara);
	            //membuat player baru
	            mp.setDataSource(afd.getFileDescriptor(),afd.getStartOffset(),afd.getLength());
	            mp.prepare();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
			
			if (mp.isPlaying()) {
				mp.stop();
			} else {
				
				mp.start();
			
			}
			mp.setOnCompletionListener(new OnCompletionListener() {
				
				@Override
				public void onCompletion(MediaPlayer arg0) {
					btn_Id.setBackgroundResource(R.drawable.btn_player_play);
					
				}
			});
		}else if (id == R.id.deskPlayerEn){
			txt_ejaan.setText(en_ejaan);
			btn_En.setBackgroundResource(R.drawable.btn_player_pause);
			
			try {
	            mp.reset();
	            AssetFileDescriptor afd = getAssets().openFd("audio/"+en_suara);
	            mp.setDataSource(afd.getFileDescriptor(),afd.getStartOffset(),afd.getLength());
	            mp.prepare();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
			
//			if (mp.isPlaying()) {
//				mp.stop();
//			} else {
//				
				mp.start();
			
//			}
			mp.setOnCompletionListener(new OnCompletionListener() {
				
				@Override
				public void onCompletion(MediaPlayer arg0) {
					btn_En.setBackgroundResource(R.drawable.btn_player_play);
					
				}
			});
		}if (id == R.id.deskPlayerAr){
			txt_ejaan.setText(ar_ejaan);
			btn_Ar.setBackgroundResource(R.drawable.btn_player_pause);
			
			try {
	            mp.reset();
	            AssetFileDescriptor afd = getAssets().openFd("audio/"+ar_suara);
	            mp.setDataSource(afd.getFileDescriptor(),afd.getStartOffset(),afd.getLength());
	            mp.prepare();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
//			
//			if (mp.isPlaying()) {
//				mp.stop();
//			} else {
				
				mp.start();
			
//			}
			mp.setOnCompletionListener(new OnCompletionListener() {
				
				@Override
				public void onCompletion(MediaPlayer arg0) {
					btn_Ar.setBackgroundResource(R.drawable.btn_player_play);
					
				}
			});
		}
	}
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem menuItem) {
	    switch (menuItem.getItemId()) {
	        case android.R.id.home:
	           finish();
	    }
	    return (super.onOptionsItemSelected(menuItem));
	}
	
	
}
