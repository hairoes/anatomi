package com.nuruldwifebriliyanti.skripsi;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.nuruldwifebriliyanti.skripsi.adapter.ListAdapter;
import com.nuruldwifebriliyanti.skripsi.database.DataHelper;
import com.nuruldwifebriliyanti.skripsi.model.Anatomi;

public class Fragment2 extends Fragment {
	private ListView lvanatomi;
	private ListAdapter adapter;
	private List<Anatomi> listanatomi;
	private DataHelper datahelper;
	 public Fragment2() {
	        // Required empty public constructor
	    }
	 
	    @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);

	    }
	 
	    @Override
	    public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                             Bundle savedInstanceState) {
	        View view = inflater.inflate(R.layout.anatomi_layout, container, false);
	        lvanatomi = (ListView)view.findViewById(R.id.lv_main);
	        datahelper = new DataHelper(getContext());
	        listanatomi = datahelper.getListAnatomi(datahelper.QUERY_DALAM);
	        adapter = new ListAdapter(getContext(), listanatomi);
	        lvanatomi.setAdapter(adapter);
	        lvanatomi.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
						long arg3) {
					Anatomi anatomi = listanatomi.get(arg2);
					Intent intent = new Intent(getContext(), Deskripsi.class);

					intent.putExtra("gambar", 	anatomi.getGambar());
					intent.putExtra("id_suara", 	anatomi.getId_suara());
					intent.putExtra("id_ejaan", 	anatomi.getId_ejaan());

					intent.putExtra("en_suara", 	anatomi.getEn_suara());
					intent.putExtra("en_ejaan", 	anatomi.getEn_ejaan());

					intent.putExtra("ar_suara", 	anatomi.getAr_suara());
					intent.putExtra("ar_ejaan", 	anatomi.getAr_ejaan());
					intent.putExtra("nama", 	anatomi.getNama());
					startActivity(intent);
				}
	        	
			});
			return view;
	        
	    }
}
