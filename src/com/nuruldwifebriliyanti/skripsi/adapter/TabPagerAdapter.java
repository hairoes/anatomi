package com.nuruldwifebriliyanti.skripsi.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.nuruldwifebriliyanti.skripsi.Fragment1;
import com.nuruldwifebriliyanti.skripsi.Fragment2;

public class TabPagerAdapter extends FragmentPagerAdapter {

	public TabPagerAdapter(FragmentManager fm) {
		super(fm);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Fragment getItem(int arg0) {
		switch (arg0) {
		case 0:
		return new Fragment1();
		case 1:
		return new Fragment2();
		default:
		break;
		}
		return null;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 2;
	}
    
}