package com.nuruldwifebriliyanti.skripsi.database;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.nuruldwifebriliyanti.skripsi.BuildConfig;
import com.nuruldwifebriliyanti.skripsi.model.Anatomi;
import com.nuruldwifebriliyanti.skripsi.model.Soal;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

public class DataHelper extends SQLiteOpenHelper{
	private Context mContext;
	private SQLiteDatabase mDatabase;
	
	private static final String DBNAME = "anatomi.db";
	public final String QUERY_LUAR = "SELECT * FROM table_anatomi WHERE id_menu = 1";
	public final String QUERY_DALAM = "SELECT * FROM table_anatomi WHERE id_menu = 2";
	
	
	
	public DataHelper(Context context){
		super(context, DBNAME, null, 1);
		this.mContext = context;
		boolean dbexist = checkDatabase();
	    if (dbexist) {
	        System.out.println("Database exists");
	        openDatabase(); 
	    } else {
	        System.out.println("Database doesn't exist");
	        createDatabase();
	    }
	}
	
	@Override
	public void onCreate(SQLiteDatabase arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}
	
	private boolean checkDatabase() {

	    boolean checkdb = false;
	    try {
	        File dbfile = new File("data/data/"+mContext.getPackageName()+"/databases/"+DBNAME);
	        checkdb = dbfile.exists();
	    } catch(SQLiteException e) {
	        System.out.println("Database doesn't exist");
	    }
	    return checkdb;
	}
	
	private void createDatabase(){
	    boolean dbexist = checkDatabase();
	    if(dbexist) {
	        System.out.println(" Database exists.");
	    } else {
	        this.getReadableDatabase();
	        copyDatabase();
	    }
	}   
	
	
	private void copyDatabase () {
    	try {
    		String path  = "data/data/"+mContext.getPackageName()+"/databases/";
			InputStream is = mContext.getAssets().open(DBNAME);
			OutputStream op = new FileOutputStream(path+DBNAME);
			byte[] buff = new byte[1024];
			int length = 0;
			while ((length  = is.read(buff)) > 0) {
				op.write(buff, 0, length);
			}
			op.flush();
			op.close();
			Toast.makeText(mContext, "Database Copied", Toast.LENGTH_SHORT).show();
			System.out.println("Database di copy.");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}  	
    }

	private void openDatabase(){
		// TODO Auto-generated method stub
		String myPath = "data/data/"+mContext.getPackageName()+"/databases/";
		if (mDatabase != null && mDatabase.isOpen()){
			return;
		}
		mDatabase = SQLiteDatabase.openDatabase(myPath+DBNAME, null, SQLiteDatabase.OPEN_READWRITE);
	}
	
	public void closeDatabase() {
		// TODO Auto-generated method stub
		if (mDatabase != null){
			mDatabase.close();
		}
	}
	
	public List<Anatomi> getListAnatomi(String Query){
		Anatomi anatomi = null;
		List<Anatomi> anatomilist = new ArrayList<Anatomi>();
		Cursor cursor =  mDatabase.rawQuery(Query, null);
		if (cursor.moveToFirst()){
			do {
				anatomi = new  Anatomi(cursor.getInt(0), 
						cursor.getString(1), 
						cursor.getString(2), 
						cursor.getString(3), 
						cursor.getString(4),
						cursor.getString(5),
						cursor.getString(6),
						cursor.getString(7),
						cursor.getString(8));
				anatomilist.add(anatomi);
			} while (cursor.moveToNext());
		}
		cursor.close();
		closeDatabase();
 		return anatomilist;
		
	}
	
	public List<Soal> getSoal(){
		Soal soal = null;
        List<Soal> listSoal = new ArrayList<Soal>();
        String query = "SELECT * FROM table_kuis";
        Cursor cursor = mDatabase.rawQuery(query, null);
        if (cursor.moveToFirst()){
        	do {
        		soal = new Soal(cursor.getString(1),
						cursor.getString(2),
						cursor.getString(3),
						cursor.getString(4),
						cursor.getString(5),
						cursor.getString(6));
                listSoal.add(soal);
			} while (cursor.moveToNext());
        	Collections.shuffle(listSoal);
        }

		cursor.close();
		closeDatabase();
        return listSoal;
    }

}
