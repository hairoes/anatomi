package com.nuruldwifebriliyanti.skripsi.model;

public class Anatomi {
	
	private int id;
	private String nama;
	private String gambar;
	private String id_suara;
	private String id_ejaan;
	private String en_suara;
	private String en_ejaan;
	private String ar_suara;
	private String ar_ejaan;
	public Anatomi(int id, String nama, String gambar, String id_suara,
			String id_ejaan, String en_suara, String en_ejaan, String ar_suara,
			String ar_ejaan) {
		super();
		this.id = id;
		this.nama = nama;
		this.gambar = gambar;
		this.id_suara = id_suara;
		this.id_ejaan = id_ejaan;
		this.en_suara = en_suara;
		this.en_ejaan = en_ejaan;
		this.ar_suara = ar_suara;
		this.ar_ejaan = ar_ejaan;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getGambar() {
		return gambar;
	}
	public void setGambar(String gambar) {
		this.gambar = gambar;
	}
	public String getId_suara() {
		return id_suara;
	}
	public void setId_suara(String id_suara) {
		this.id_suara = id_suara;
	}
	public String getId_ejaan() {
		return id_ejaan;
	}
	public void setId_ejaan(String id_ejaan) {
		this.id_ejaan = id_ejaan;
	}
	public String getEn_suara() {
		return en_suara;
	}
	public void setEn_suara(String en_suara) {
		this.en_suara = en_suara;
	}
	public String getEn_ejaan() {
		return en_ejaan;
	}
	public void setEn_ejaan(String en_ejaan) {
		this.en_ejaan = en_ejaan;
	}
	public String getAr_suara() {
		return ar_suara;
	}
	public void setAr_suara(String ar_suara) {
		this.ar_suara = ar_suara;
	}
	public String getAr_ejaan() {
		return ar_ejaan;
	}
	public void setAr_ejaan(String ar_ejaan) {
		this.ar_ejaan = ar_ejaan;
	}
	
	
	
	
	
}
