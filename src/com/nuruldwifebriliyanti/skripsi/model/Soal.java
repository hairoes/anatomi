package com.nuruldwifebriliyanti.skripsi.model;

public class Soal {
	private String soal;
    private String pil_a;
    private String pil_b;
    private String pil_c;
    private String jwban;
    private String gambar;
    
    
	public Soal(String soal, String pil_a, String pil_b, String pil_c,
			String jwban, String gambar) {
		super();
		this.soal = soal;
		this.pil_a = pil_a;
		this.pil_b = pil_b;
		this.pil_c = pil_c;
		this.jwban = jwban;
		this.gambar = gambar;
	}
	public String getSoal() {
		return soal;
	}
	public void setSoal(String soal) {
		this.soal = soal;
	}
	public String getPil_a() {
		return pil_a;
	}
	public void setPil_a(String pil_a) {
		this.pil_a = pil_a;
	}
	public String getPil_b() {
		return pil_b;
	}
	public void setPil_b(String pil_b) {
		this.pil_b = pil_b;
	}
	public String getPil_c() {
		return pil_c;
	}
	public void setPil_c(String pil_c) {
		this.pil_c = pil_c;
	}
	public String getJwban() {
		return jwban;
	}
	public void setJwban(String jwban) {
		this.jwban = jwban;
	}
	public String getGambar() {
		return gambar;
	}
	public void setGambar(String gambar) {
		this.gambar = gambar;
	}
}
